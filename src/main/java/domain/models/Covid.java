package domain.models;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import sun.util.resources.cldr.da.TimeZoneNames_da;

import java.io.IOException;

public class Covid {
    private Document document;

    public Covid(){
        connect();
    }

    private void connect(){
        try {
            document = Jsoup.connect("https://www.worldometers.info/coronavirus").get();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public String getCountry(String country){
        try {
            document = Jsoup.connect("https://www.worldometers.info/coronavirus/country/" + country + "/").get();
        }catch(IOException e){
            e.printStackTrace();
        }
        return document.title();
    }

    public String getTop(){
        try {
            document = Jsoup.connect("https://www.worldometers.info/coronavirus").get();
        }catch(IOException e){
            e.printStackTrace();
        }
        Elements elements = document.getElementsByClass("mt_a");
        String info = elements.text();
        String top = ""; int ind = 0; int cnt = 1;
        for(int i = 0 ; i < 60 ; i ++){
            if(info.charAt(i) == ' '){
                top+=cnt + ". " + info.substring(ind , i) + "\n";
                ind = i;
                cnt++;
            }
        }
        return top;
    }


    public String getTotalCases(){
        Elements elements = document.getElementsByClass("maincounter-number");
        return elements.text();
    }


}
