package domain.models;

import java.sql.Date;

public class Symptom {

    private long id;
    private String name;
    private String definition;


    public Symptom() {

    }

    public Symptom(String name, String definition) {
        this.name = name;
        this.definition = definition;
    }

    public Symptom(long id, String name, String definition) {
        this.id = id;
        this.name = name;
        this.definition = definition;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefinition() { return definition; }

    public void setDefinition(String definition) { this.definition = definition; }

    @Override
    public String toString() {
        return name + " " +
                "\n" + definition;
    }
}
