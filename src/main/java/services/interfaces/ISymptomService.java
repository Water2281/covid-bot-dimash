package services.interfaces;

import domain.models.Symptom;

public interface ISymptomService {
    Symptom getSymptomByID(long id);

    Symptom getSymptomByName(String name);

    void addSymptom(Symptom symptom);

}
